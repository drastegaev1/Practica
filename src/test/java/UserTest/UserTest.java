package UserTest;

import Generations.UserGenerator;
import Services.UserService;
import dto.User;
import dto.UserOut;
import io.restassured.response.Response;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

public class UserTest {
    @Test                                           // Обязательно
    public void createUserTest() {               //Любое уникальное наименование метода, не обязательно createUserTest
      /*  User user = User.builder()                // Вариант использования единичного заведения
                .id(1L)
                .email("EMAIL")
                .firstName("first Name")
                .lastName("lastName")
                .password("password")
                .phone("phone")
                .username("username")
                .userStatus(2L)
                .build();
*/
        User user = UserGenerator.getNewUser();     //Вариант использования генератора

        UserService userService = new UserService();            //Описание функции
        Response response = userService.CreatUser(user);

        UserOut actual = response.then()             //Описание функции actual
                .log()
                .all()
                .statusCode(200)
                .extract()
                .body()
                .as(UserOut.class);

        UserOut expected = UserOut.builder()                 //Описание функции expected
                .code(200L)
                .message(user.getId().toString())
                .type("unknown")
                .build();
        Assertions.assertEquals(expected, actual);


    }

    @ParameterizedTest
    // 2 вариант запуска тестов и использованием ролей
    @ValueSource(strings = {"admin", "moderator", "user"})
    public void getUserByName(String name) {
        UserService userService = new UserService();
        Response response = userService.GetUserByName(name);

        UserOut actual = response.then()             //Описание функции actual
                .log()
                .all()
                .statusCode(404)
                .extract()
                .body()
                .as(UserOut.class);

        UserOut expected = UserOut.builder()                 //Описание функции expected
                .code(1L)
                .message("User not found")
                .type("error")
                .build();
        Assertions.assertEquals(expected, actual);
    }
}
