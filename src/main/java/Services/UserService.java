package Services;

import com.atlassian.oai.validator.restassured.OpenApiValidationFilter;
import dto.User;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import io.restassured.specification.ResponseSpecification;

import static io.restassured.RestAssured.given;

public class UserService {
    private OpenApiValidationFilter filter = new OpenApiValidationFilter("https://petstore.swagger.io/v2/swagger.json");
    private RequestSpecification spec;

    public UserService(){
        spec = given()
                .baseUri("https://petstore.swagger.io/v2")
                .filter(filter)
                .log().all()
                .contentType(ContentType.JSON);

    }

    public Response CreatUser(User user){
        return given(spec)
            //    .baseUri("https://petstore.swagger.io/v2")
            //    .log().all()
            //    .contentType(ContentType.JSON)
                .body(user)
                .post("/user");

    }
    public Response GetUserByName(String name){
        return given(spec)
           //     .baseUri("https://petstore.swagger.io/v2")
           //     .filter()
           //     .log().all()
            //    .contentType(ContentType.JSON)
                .when()
                .get("/user/" + name);
    }
}
