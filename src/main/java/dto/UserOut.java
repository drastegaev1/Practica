
package dto;

import lombok.*;
import org.codehaus.jackson.map.annotate.JsonSerialize;

@Getter
@Builder
@JsonSerialize
@AllArgsConstructor
@NoArgsConstructor
@ToString
@EqualsAndHashCode
public class UserOut {

    private Long code;
    private String message;
    private String type;


}
