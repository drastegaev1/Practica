
package dto;

import lombok.*;
import org.codehaus.jackson.map.annotate.JsonSerialize;

@Setter
@Getter
@Builder
@JsonSerialize
@EqualsAndHashCode
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class User {

    private String email;
    private String firstName;
    private Long id;
    private String lastName;
    private String password;
    private String phone;
    private Long userStatus;
    private String username;

}
