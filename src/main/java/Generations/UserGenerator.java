package Generations;

import dto.User;

public class UserGenerator {
    private static long COUNT= 0;

    public static User getNewUser(){
        COUNT++;
        return User.builder()
                .id(COUNT)
                .email("EMAIL")
                .firstName("first Name")
                .lastName("lastName")
                .password("password" + COUNT)
                .phone("phone")
                .username("username" + COUNT)
                .userStatus(2L)
                .build();
    }
    public static User getExistUser() {
        return User.builder()
                .id(COUNT)
                .email("EMAIL")
                .firstName("First Name")
                .lastName("Last Name")
                .password("PASSWORD" + COUNT)
                .phone("8(999)999-99-99")
                .username("USERNAME" + COUNT)
                .userStatus(2L)
                .build();
    }
}
